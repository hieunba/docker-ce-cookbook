# frozen_string_literal: true

module DockerCeCookbook
  module Helper
    include Chef::Mixin::ShellOut

    def node_distribution
      case node['platform']
      when 'debian'
        # rubocop:disable Style/StringLiterals
        debvers = shell_out("cat /etc/os-release | grep \"VERSION=\"").stdout
        # rubocop:enable Style/StringLiterals

        raise 'Cannot determine the current distribution!' unless debvers

        debvers.scan(/\d+\s\(([a-z]+)\)/).flatten.first
      when 'ubuntu'
        shell_out('lsb_release -cs').stdout.gsub(/\n/, '')
      end
    end
  end
end
