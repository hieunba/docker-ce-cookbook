# docker-ce-cookbook CHANGELOG

This file is used to list changes made in each version of this cookbook.

## Unreleased


# 0.11.0 (2020-05-06)

 - Adds support for Ubuntu 18.04
 - Shows cookbook name in log messages

# 0.10.0 (2020-03-09)

 - Adds support for Debian 10 Buster

# 0.9.0 (2019-11-06)

  - Allows changing the log-driver and setting of the live-restore option

# 0.8.0 (2019-08-12)

 - Allows chaning the Docker native.cgroupdriver to systemd

# 0.7.0 (2019-08-09)

 - Allows setting docker daemon 'registry-mirrors' setting

# 0.6.0 (2019-08-07)

 - Implements Docker upgrade
 - Optimise the recipe
 - Lock the docker packages after install and upgrade

# 0.5.0 (2019-07-30)

 - Adds the apt cookbook as dependency and removes useless apt-get update commands

# 0.4.0 (2019-07-27)

 - Removes useless `apt-get update` commands

# 0.3.0 (2019-07-26)

 - Adds Docker CE version attribute to define the APT package version to be installed.

# 0.2.0 (2019-05-27)

 - Debian 8 support
 - ChefSpecs

# 0.1.0 (2019-05-23)

Initial release.
