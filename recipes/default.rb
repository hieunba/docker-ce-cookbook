# frozen_string_literal: true

#
# Cookbook:: docker-ce-cookbook
# Recipe:: default
#
# Copyright: 2019, Pharmony SA, All Rights Reserved.
#

# `node_distribution` in the recipe body (See recipe bottom)
Chef::Recipe.send(:include, DockerCeCookbook::Helper)
# `packages_to_be_installed` in the recipe body (See recipe bottom)
Chef::Recipe.send(:include, DockerCeCookbook::AptPackageHelper)
# `build_docker_version` in `package`
# `upgrading_docker?` in the recipe body (See recipe bottom)
Chef::Resource::AptPackage.send(:include, DockerCeCookbook::AptPackageHelper)
# `node_distribution` in `apt_repository`
Chef::Resource::AptRepository.send(:include, DockerCeCookbook::Helper)

include_recipe 'apt'

#
# Clean up
#
package %w[
  docker
  docker-engine
  docker.io
  containerd
  runc
] do
  action :remove
  # Do not remove docker when it is already install, otherwise it would
  # shut it down, which you don't want in prod :)
  # So this package block is necessary only for empty boxes where you'd like
  # to install Docker for the first time.
  only_if { shell_out('which docker').stdout.empty? }
end

#
# Dependencies
#
package %w[
  apt-transport-https
  ca-certificates
  curl
  gnupg2
  software-properties-common
] do
  action :install
end

#
# Docker APT repo
#
apt_repository 'docker' do
  uri           "https://download.docker.com/linux/#{node['platform']}"
  arch          'amd64'
  components    ['stable']
  distribution  node_distribution
  key           "https://download.docker.com/linux/#{node['platform']}/gpg"
  action        :add
  not_if 'test -f /etc/apt/sources.list.d/docker.list'
end

#
# Docker Installation
#
packages_to_be_installed.each do |name|
  # When package needs to just be installed
  package name do
    version(lazy { name == 'containerd.io' ? nil : build_docker_version })
    action %i[install lock]
    not_if { upgrading_docker? }
  end

  # When package needs to be upgraded
  package name do
    version(lazy { name == 'containerd.io' ? nil : build_docker_version })
    action %i[unlock upgrade lock]
    only_if { upgrading_docker? }
  end
end

#
# Docker Configuration
#
if node['docker_ce']['daemon']
  directory '/etc/docker' do
    owner 'root'
    group 'root'
    mode '0755'
  end

  if Array(node['docker_ce']['daemon']['registry_mirrors']).empty? &&
     node['docker_ce']['daemon']['cgroup_driver'] != 'systemd' &&
     node['docker_ce']['daemon']['live_restore'] == false &&
     node['docker_ce']['daemon']['log_driver'] == 'json-file'
    file '/etc/docker/daemon.json' do
      action :delete
      notifies :restart, 'service[docker]', :immediately
    end
  else
    template '/etc/docker/daemon.json' do
      source 'daemon.json.erb'
      owner 'root'
      group 'root'
      mode '0600'
      notifies :restart, 'service[docker]', :immediately
    end
  end

  service 'docker' do
    action [:nothing]
    supports [restart: true]
  end
end
