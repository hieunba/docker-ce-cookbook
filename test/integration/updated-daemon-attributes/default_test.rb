# frozen_string_literal: true

# Inspec test for recipe docker-ce-cookbook::default

# The Inspec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

describe directory('/etc/docker') do
  it { should exist }
  its('owner') { should eq 'root' }
  its('group') { should eq 'root' }
  its('mode') { should cmp '0755' }
end

describe file('/etc/docker/daemon.json') do
  it { should exist }
  its('owner') { should eq 'root' }
  its('group') { should eq 'root' }
  its('mode') { should cmp '0600' }
  its('content') do
    should eq(
      <<~JSON
        {
          "exec-opts": [
            "native.cgroupdriver=systemd"
          ],
          "registry-mirrors": [
            "http://localhost:5000"
          ]
        }
      JSON
    )
  end
end
